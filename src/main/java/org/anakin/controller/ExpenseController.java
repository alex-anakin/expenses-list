package org.anakin.controller;

import org.anakin.entity.Expense;
import org.anakin.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class ExpenseController {

	@Autowired
	private ExpenseRepository repository;

	@GetMapping(value = "/expenses")
	public Iterable<Expense> getExpenses() {
		return repository.findAll();
	}

	@GetMapping(value = "/expenses/{id}")
	public Expense getExpense(@PathVariable Long id) {
		return repository.findOne(id);
	}

	@PostMapping(value = "/expenses")
	public Expense saveExpense(@RequestBody Expense expense) {
		return repository.save(expense);
	}
}
