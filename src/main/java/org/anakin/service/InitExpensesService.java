package org.anakin.service;

import org.anakin.entity.Expense;
import org.anakin.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Service
public class InitExpensesService {

	@Autowired
	private ExpenseRepository repository;

	@PostConstruct
	public void init() {
		System.out.println("Starting init expenses data ...");
		repository.save(new Expense("Tomatoes","2017-06-01", BigDecimal.valueOf(44.50)));
		repository.save(new Expense("Cucumbers","2017-06-01", BigDecimal.valueOf(20.75)));
		repository.save(new Expense("Onion","2017-06-01", BigDecimal.valueOf(12.00)));
		repository.save(new Expense("Salad","2017-06-01", BigDecimal.valueOf(25.00)));
		repository.save(new Expense("Sugar","2017-06-02", BigDecimal.valueOf(17.99)));
		repository.save(new Expense("Olive oil","2017-06-02", BigDecimal.valueOf(259.99)));
		repository.save(new Expense("Feta","2017-06-02", BigDecimal.valueOf(32.35)));
		repository.save(new Expense("Donuts","2017-06-02", BigDecimal.valueOf(28.70)));
		repository.save(new Expense("Pork chops","2017-06-02", BigDecimal.valueOf(121.47)));
		System.out.println("Finished init expenses data!");
	}
}
