package org.anakin.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class Expense {

	public Expense(String name, String date, BigDecimal amount) {
		this.name = name;
		this.date = date;
		this.amount = amount;
	}

	public Expense() {
	}

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long id;

	public String name;
	public String date;
	public BigDecimal amount;
}
