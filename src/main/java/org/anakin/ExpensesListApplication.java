package org.anakin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpensesListApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpensesListApplication.class, args);
	}
}
